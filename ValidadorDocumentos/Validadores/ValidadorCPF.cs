﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidadorDocumentos.Validadores
{
    public class ValidadorCPF : ValidadorDocumentos
    {
         public ValidadorCPF()
         {
            MultiplicadorReiniciaNo10 = false;
            QuantidadeDigitosVerificadores = 2;
         }
    }
}

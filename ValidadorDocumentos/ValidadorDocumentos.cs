using System.Globalization;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace ValidadorDocumentos
{
    public class ValidadorDocumentos : IValidadorDocumentos
    {
        private const int MODULO = 11;

        protected internal bool MultiplicadorReiniciaNo10 { get; set; } 
        protected internal int QuantidadeDigitosVerificadores { get; set; }
        
        public bool Validar(string Numero)
        {
            if (IsNull(Numero)) return false;
            Numero = SoNumeros(Numero);   

            if (Numero.Length <= QuantidadeDigitosVerificadores) return false;
            if (NumerosRepetidos(Numero)) return false;
            
            int[] Numeros = NumeroStrToIntArray(Numero);


            try
            {
                for (int i = 1; i <= QuantidadeDigitosVerificadores; i++)
                {

                    int PosicaoDV = Numeros.Length - i;

                    int DV = CalcularModulo11(Numeros, PosicaoDV - 1);
                    if (DV != Numeros[PosicaoDV]) return false;
                }
            }
            catch
            {
                return false;
            }


            return true;

        }
       
    
        private int CalcularModulo11(int[] Numeros, int PosicaoCalculo)
        {
            
             int soma = 0;
             
             for(int i = PosicaoCalculo, Multiplicador = 2; i >= 0; i--, Multiplicador++)
             {
                   if (Multiplicador == 10)
                        if (MultiplicadorReiniciaNo10)
                            Multiplicador = 2;

                soma += Numeros[i] * Multiplicador;
             }
            
            var resultado = soma % MODULO;
            if (resultado < 2)
               return 0;
            else return  MODULO - resultado;

        } 

        private int[] NumeroStrToIntArray(string Numero)
        { 
           return  Numero.ToCharArray().Select(n => Convert.ToInt32(n.ToString())).ToArray();
        }


        private bool IsNull(string Numero)
        {
            return String.IsNullOrEmpty(Numero);
        }

        private bool NumerosRepetidos(string Numero)
        {
            Numero = SoNumeros(Numero);
            for(int i = 0;i<=9;i++)
            {
                string repetidos = "";
                for(int x = 0; x < Numero.Length; x++)
                {
                    repetidos += i.ToString();
                }
                if (Numero.Equals(repetidos))
                   return true;
            }

            return false;
        }

        private string SoNumeros(string Numero)
        {
            if (IsNull(Numero)) return "";

            string retorno = "";
            char[] array = Numero.ToCharArray();
            foreach(char item in array)
            {
                if (Regex.IsMatch(item.ToString(), @"^\d+$"))
                    retorno += item.ToString();
            }
            return retorno;
        }
    }
}
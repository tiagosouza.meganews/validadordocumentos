﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidadorDocumentos
{
    public enum TipoDocumento { CNPJ,CPF,PIS, RENAVAM}

    public abstract class ValidadorDocumentosFactory
    {
        public abstract IValidadorDocumentos GetValidadorDocumentos(TipoDocumento TipoDocumento);
    }
}

using Xunit;
namespace ValidadorDocumentos.tests
{
    public class ValidadorDocumentosCNPJTestes
    {


        [Fact]
        public void ValidarCNPJNullFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar(null));
        }


        [Fact]
        public void ValidarCNPJComPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.True(IValidadorCNPJ.Validar("11.222.333/0001-81"));
        }

        [Fact]
        public void ValidarCNPJSemPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.True(IValidadorCNPJ.Validar("11222333000181"));
        }

        [Fact]
        public void ValidarCNPJComLetrasFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("a1222333000181"));
        }

        [Fact]
        public void ValidarCNPJTamanhoInvalidoFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("1222333000181"));
        }

        [Fact]
        public void ValidarCNPJRepetidos0000000000000False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("0000000000000"));
        }

        [Fact]
        public void ValidarCNPJRepetidos1111111111111False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("1111111111111"));
        }

        [Fact]
        public void ValidarCNPJRepetidos2222222222222False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("2222222222222"));
        }

        [Fact]
        public void ValidarCNPJRepetidos3333333333333False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("3333333333333"));
        }

        [Fact]
        public void ValidarCNPJRepetidos4444444444444False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("4444444444444"));
        }

        [Fact]
        public void ValidarCNPJRepetidos5555555555555False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("5555555555555"));
        }

        [Fact]
        public void ValidarCNPJRepetidos6666666666666False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("6666666666666"));
        }

        [Fact]
        public void ValidarCNPJRepetidos7777777777777False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("7777777777777"));
        }

        [Fact]
        public void ValidarCNPJRepetidos8888888888888False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("8888888888888"));
        }

        [Fact]
        public void ValidarCNPJRepetidos9999999999999False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCNPJ = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CNPJ);
            Assert.False(IValidadorCNPJ.Validar("9999999999999"));
        }
    }

}
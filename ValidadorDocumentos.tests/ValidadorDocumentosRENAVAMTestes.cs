﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ValidadorDocumentos.tests
{
    public class ValidadorDocumentosRENAVAMTestes
    {
        [Fact]
        public void ValidarRENAVAMNullFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar(null));
        }


        [Fact]
        public void ValidarRENAVAMComPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.True(IValidadorRENAVAM.Validar("15248848265"));
        }

        [Fact]
        public void ValidarRENAVAMSemPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.True(IValidadorRENAVAM.Validar("28714555851"));
        }

        [Fact]
        public void ValidarRENAVAMComLetrasFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("AA109070284"));
        }

        [Fact]
        public void ValidarRENAVAMTamanhoInvalidoFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("723061873932"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos000000000000False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("000000000000"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos111111111111False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("111111111111"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos222222222222False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("222222222222"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos333333333333False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("333333333333"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos444444444444False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("444444444444"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos555555555555False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("555555555555"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos666666666666False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("666666666666"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos777777777777False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("777777777777"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos888888888888False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("888888888888"));
        }

        [Fact]
        public void ValidarRENAVAMRepetidos999999999999False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorRENAVAM = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.RENAVAM);
            Assert.False(IValidadorRENAVAM.Validar("999999999999"));
        }
    }
}

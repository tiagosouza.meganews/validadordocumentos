﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ValidadorDocumentos.tests
{
    public class ValidadorDocumentosPISTestes
    {
        [Fact]
        public void ValidarPISNullFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar(null));
        }


        [Fact]
        public void ValidarPISComPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.True(IValidadorPIS.Validar("233.92263.04-0"));
        }

        [Fact]
        public void ValidarCPFSemPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.True(IValidadorPIS.Validar("23392263040"));
        }

        [Fact]
        public void ValidarCPFComLetrasFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("aa3392263040"));
        }

        [Fact]
        public void ValidarCPFTamanhoInvalidoFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("233922630400"));
        }

        [Fact]
        public void ValidarCPFRepetidos00000000000False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("00000000000"));
        }

        [Fact]
        public void ValidarCPFRepetidos11111111111False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("11111111111"));
        }

        [Fact]
        public void ValidarCPFRepetidos22222222222False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("22222222222"));
        }

        [Fact]
        public void ValidarCPFRepetidos33333333333False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("33333333333"));
        }

        [Fact]
        public void ValidarCPFRepetidos44444444444False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("44444444444"));
        }

        [Fact]
        public void ValidarCPFRepetidos55555555555False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("55555555555"));
        }

        [Fact]
        public void ValidarCPFRepetidos66666666666False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("66666666666"));
        }

        [Fact]
        public void ValidarCPFRepetidos77777777777False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("77777777777"));
        }

        [Fact]
        public void ValidarCPFRepetidos88888888888False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("88888888888"));
        }

        [Fact]
        public void ValidarCPFRepetidos99999999999False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorPIS = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.PIS);
            Assert.False(IValidadorPIS.Validar("99999999999"));
        }

    }
}

namespace ValidadorDocumentos.Validadores
{
    public class ValidadorCNPJ : ValidadorDocumentos
    {
        public ValidadorCNPJ()
        {
            MultiplicadorReiniciaNo10 = true;
            QuantidadeDigitosVerificadores = 2;
        }
    }
}
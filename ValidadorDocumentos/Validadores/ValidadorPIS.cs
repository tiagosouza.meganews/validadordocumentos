﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidadorDocumentos.Validadores
{
    public class ValidadorPIS : ValidadorDocumentos
    {
        public ValidadorPIS()
        {
            MultiplicadorReiniciaNo10 = true;
            QuantidadeDigitosVerificadores = 1;
         }
    }
}

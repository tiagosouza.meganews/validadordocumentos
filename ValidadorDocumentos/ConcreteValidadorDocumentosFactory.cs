﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidadorDocumentos.Validadores;

namespace ValidadorDocumentos
{
    public class ConcreteValidadorDocumentosFactory : ValidadorDocumentosFactory
    {
        public override IValidadorDocumentos GetValidadorDocumentos(TipoDocumento TipoDocumento)
        {
            switch(TipoDocumento)
            {
                case TipoDocumento.CPF:
                    return new ValidadorCPF();
                case TipoDocumento.CNPJ:
                    return new ValidadorCNPJ();
                case TipoDocumento.PIS:
                    return new ValidadorPIS();
                case TipoDocumento.RENAVAM:
                    return new  ValidadorRENAVAM();
                default:
                    throw new NotImplementedException($"Tipo de Documento não implementado");
            }
        }
    }
}

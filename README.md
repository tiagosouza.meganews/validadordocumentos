## DIAGRAMA DE CLASSES ##

![alt text](DiagramaClasses.png)

O projeto é capaz de validar qualquer documento que use o módulo 11 para validar os digitos verificadores.

### Para criar uma nova classe de validação: ###
1. Crie uma classe na pasta ```Validadores``` herdando de ```ValidadorDocumentos```
```cs
ValidadorCPF : ValidadorDocumentos{
       
}
```
   
2. Crie um construtor preenchendo as duas propriedades: ```MultiplicadorReiniciaNo10``` e ```QuantidadeDigitosVerificadores```
```cs
ValidadorCPF : ValidadorDocumentos{
   public ValidadorCPF()
   {
      MultiplicadorReiniciaNo10 = false;
      QuantidadeDigitosVerificadores = 2;
   }
}
```
3. Vá na classe ```ConcreteValidadorDocumentoFactory``` e no switch acrescente uma nova opção instanciando a classe criada anteriormente
```cs
switch(TipoDocumento)
{
   case TipoDocumento.CPF:
      return new ValidadorCPF();
   case TipoDocumento.CNPJ:
      return new ValidadorCNPJ();
   case TipoDocumento.PIS:
      return new ValidadorPIS();
   case TipoDocumento.RENAVAM:
      return new  ValidadorRENAVAM();
   default:
     throw new NotImplementedException($"Tipo de Documento não implementado");
}

```
4. Para instanciar a fabrica:
```cs
//INSTANCIA DA FÁBRICA
ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();

```
5. Para chamar uma validação:
```cs
//INSTÂNCIA do IValidadorDocumentos
IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);

bool valido = IValidadorCPF.Validar("123.456.789-10");
```
6. O projeto está usando o xUnit para testar as classes. Para criar os testes vá no projeto ```ValidadorDocumentos.tests``` e crie uma nova classe de validação, escreva os testes instanciando a fabrica
ex.:
```cs
 [Fact]
 public void ValidarCPFNullFalse()
 {
   ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
   IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
   Assert.False(IValidadorCPF.Validar(null));
 }

```


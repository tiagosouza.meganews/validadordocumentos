﻿using Xunit;

namespace ValidadorDocumentos.tests
{
    public class ValidadorDocumentosCPFTestes
    {
        [Fact]
        public void ValidarCPFNullFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar(null));
        }


        [Fact]
        public void ValidarCPFComPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.True(IValidadorCPF.Validar("060.805.350-33"));
        }

        [Fact]
        public void ValidarCPFSemPontosTrue()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.True(IValidadorCPF.Validar("06080535033"));
        }

        [Fact]
        public void ValidarCPFComLetrasFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("aA080535033"));
        }

        [Fact]
        public void ValidarCPFTamanhoInvalidoFalse()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("060805350335"));
        }

        [Fact]
        public void ValidarCPFRepetidos00000000000False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("00000000000"));
        }

        [Fact]
        public void ValidarCPFRepetidos11111111111False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("11111111111"));
        }

        [Fact]
        public void ValidarCPFRepetidos22222222222False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("22222222222"));
        }

        [Fact]
        public void ValidarCPFRepetidos33333333333False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("33333333333"));
        }

        [Fact]
        public void ValidarCPFRepetidos44444444444False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("44444444444"));
        }

        [Fact]
        public void ValidarCPFRepetidos55555555555False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("55555555555"));
        }

        [Fact]
        public void ValidarCPFRepetidos66666666666False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("66666666666"));
        }

        [Fact]
        public void ValidarCPFRepetidos77777777777False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("77777777777"));
        }

        [Fact]
        public void ValidarCPFRepetidos88888888888False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("88888888888"));
        }

        [Fact]
        public void ValidarCPFRepetidos99999999999False()
        {
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();
            IValidadorDocumentos IValidadorCPF = ValidadorFactory.GetValidadorDocumentos(TipoDocumento.CPF);
            Assert.False(IValidadorCPF.Validar("99999999999"));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidadorDocumentos.Validadores
{
    public class ValidadorRENAVAM : ValidadorDocumentos
    {
        public ValidadorRENAVAM()
        {
            QuantidadeDigitosVerificadores = 1;
            MultiplicadorReiniciaNo10 = true;
        }
    }
}

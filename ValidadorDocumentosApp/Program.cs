﻿using System;
using ValidadorDocumentos;


namespace ValidadorDocumentosApp
{
    class Program
    {
        static void Main(string[] args)
        {

            //INSTANCIA DA FÁBRICA
            ValidadorDocumentosFactory ValidadorFactory = new ConcreteValidadorDocumentosFactory();











            ExibeOpcoes();
            TipoDocumento tipo = NumeroDigitadoToTipoDocumento();

            //INSTÂNCIA do IValidadorDocumentos
            IValidadorDocumentos IValidador = ValidadorFactory.GetValidadorDocumentos(tipo);

            ExibeTextoParaDigitarDocumento(tipo);


            ExibeResultado(tipo, IValidador.Validar(Console.ReadLine().ToString()));

            Console.ReadKey();

            // Console.writteli IValidador.Validar(documento);

            /* Console.WriteLine("CNPJ = 11222333000181 = " + IValidadeCNPJ.Validar("11222333000181"));
             Console.WriteLine("CPF = 11144477735 = " + IValidadorCPF.Validar("11144477735"));
             Console.WriteLine("CPF = 02134338504 =" + IValidadorCPF.Validar("02134338504"));
             Console.ReadKey();*/



        }

        static void ExibeTextoParaDigitarDocumento(TipoDocumento tipo)
        {
            Console.WriteLine($"DIGITE O {tipo}:");
        }

        private static void ExibeResultado(TipoDocumento TipoDocumento, bool Valido)
        {
            Console.WriteLine("\n");
            Console.WriteLine("**********************************************************");
            Console.WriteLine($"O {TipoDocumento} DIGITADO É: " + (Valido ? "VÁLIDO :)" : "INVÁLIDO! :("));
            Console.WriteLine("**********************************************************");

        }

        static TipoDocumento NumeroDigitadoToTipoDocumento()
        {
            Int64 numeroDigitado = Convert.ToInt64(Console.ReadLine());
            return (TipoDocumento)numeroDigitado;
        }

        static void ExibeOpcoes()
        {
            Console.WriteLine("QUAL TIPO DE DOCUMENTO DESEJA VALIDAR?");
            foreach (int i in Enum.GetValues(typeof(TipoDocumento)))
                Console.WriteLine("(" + i.ToString() + ") " + (TipoDocumento)i);


        }
    }
}
